<!--
SPDX-FileCopyrightText: (C) 2024 Hong Yongmin (https://revi.xyz/) <yewon@revi.email>

SPDX-License-Identifier: Apache-2.0 OR LicenseRef-CC-BY-2.0-KR

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Contributing

I dunno why you felt like contributing in this profile, but fine.

However; I don't like GitLab and does not accept GitLab merge request.

Use [issuetracker.revi](https://issuetracker.revi.xyz) for patch submission.
Or if you don't want to install `arc`, you can send me `git format-patch` diff,
or use Phorge's web interface to submit patch.

Anyways, I think you get the message; no merge request. Period.
