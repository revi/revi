<!--
SPDX-FileCopyrightText: (C) 2024 Hong Yongmin (https://revi.xyz/) <yewon@revi.email>

SPDX-License-Identifier: Apache-2.0 OR LicenseRef-CC-BY-2.0-KR

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Hong Yongmin (revi)

<!-- All internal links for GitLab should be made w/ full URL (rather than relative links) due to the rendering errors when displayed in profile page -->

- _In this [Korean name](https://en.wikipedia.org/wiki/Korean_name), the family name is [Hong](https://w.wiki/ANiq)._
  - GitLab profile metadata does not have my family name as too much _Western_
    people always assumed the order is FIRSTNAME LASTNAME.
- Lazy amateur translator, real newbie coder, chief laziness officer.
  - I generally don't use GitLab that much.
    Most of stuff is a mirror of repositories that is already on other git forge
    (GitHub, CodeBerg, self-hosted Forgejo).
- [he/him unless otherwise specified](https://revi.xyz/pronoun-is/).
- [Homepage](https://revi.xyz).
  - [Email address](https://revi.xyz/contact-method/#email).
    Thank spammers who scrape GitHub profile's email field for this. GitLab just got a splash damage.
- Internal links: [go/todo](http://go/todo), [go/notion](http://go/notion)
